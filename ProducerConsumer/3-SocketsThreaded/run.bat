::
:: Start the server, and start two clients. Enter a number in one of the client's 
:: consoles to see the output. -1 will stop the server.
::

start java -cp build\classes\main Server

:: Press a key to start the clients
@pause

start java -cp build\classes\main Client
start java -cp build\classes\main Client