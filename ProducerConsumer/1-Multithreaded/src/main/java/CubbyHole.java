/**
 * 
 *
 */
public class CubbyHole {
	
    private int contents;

    /**
     * 
     * @return
     */
    public int get() {
        System.out.println("  Consumer gets: " + contents);
        return contents;
    }

    /**
     * 
     * @param value
     */
    public void put(int value) {
        contents = value;
        System.out.println("Producer puts: " + contents);
    }
}

