/**
 * 
 *
 */
public class Consumer2 implements Runnable {

	private CubbyHole2 cubbyhole;

	/**
	 * 
	 * @param c
	 */
    public Consumer2(CubbyHole2 c) {
        cubbyhole = c;
    }

    /**
     * 
     */
    public void run() {
        int value = 0;
        for (int i = 1; i <= 5; i++) {
            value = cubbyhole.get();
            try {
                Thread.sleep((int)(Math.random() * 100));
            } catch (InterruptedException e) { }
        }
    }
}
