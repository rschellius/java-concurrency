/**
 * 
 *
 */
public class Producer2 implements Runnable {
    private CubbyHole2 cubbyhole;

    /**
     * 
     * @param c
     */
    public Producer2(CubbyHole2 c) {
        cubbyhole = c;
    }

    /**
     * 
     */
    public void run() {
        for (int i = 1; i <= 5; i++) {
            cubbyhole.put(i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) { }
        }
    }
}
