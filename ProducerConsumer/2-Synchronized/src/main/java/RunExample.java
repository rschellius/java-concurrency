/**
 * 
 *
 */
public class RunExample {
	
	/**
	 * 
	 * @param args
	 */
    public static void main( String[] args ) {
	CubbyHole2 c = new CubbyHole2();
	Producer2 producer = new Producer2( c );
	Consumer2 consumer = new Consumer2( c );
	new Thread(producer).start();
	new Thread(consumer).start();
    }
}
