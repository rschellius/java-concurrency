/**
 * 
 */
import java.util.concurrent.locks.*;

/**
 * 
 *
 */
public class CubbyHole2 {
	
    private int contents;
    private boolean available = false;
    private Lock aLock = new ReentrantLock();
    private Condition condVar = aLock.newCondition();

    /**
     * 
     * @return
     */
    public int get() {
        aLock.lock();
        try {
            while (available == false) {
                try {
                    condVar.await();
                } catch (InterruptedException e) { }
            }
            available = false;
            System.out.println("  Consumer gets " + contents);
            condVar.signalAll();
        } finally {
            aLock.unlock();
        }
        return contents;
    }

    /**
     * 
     * @param value
     */
    public void put(int value) {
        aLock.lock();
        try {
            while (available == true) {
                try {
                    condVar.await();
                } catch (InterruptedException e) { }
            }
            contents = value;
            available = true;
            System.out.println("Producer puts: " + contents);
            condVar.signalAll();
        } finally {
            aLock.unlock();
        }
    }
}
